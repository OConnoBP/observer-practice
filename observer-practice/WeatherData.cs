﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace observer_practice
{
    public class WeatherData : ISubject
    {
        private List<IObserver> observers;
        private float temp;
        private float humidity;
        private float pressure;

        public WeatherData()
        {
            this.observers = new List<IObserver>();
        }

        public void registerObserver( IObserver observer )
        {
            observers.Add( observer );
        }

        public void removeObserver( IObserver observer )
        {
            observers.Remove( observer );
        }

        public void notifyObservers()
        {
            foreach( IObserver observer in this.observers )
            {
                observer.update( this.temp, this.humidity, this.pressure );
            }
        }

        public void measurementsChanged()
        {
            this.notifyObservers();
        }

        public void setMeasurements( float temp, float humidity, float pressure )
        {
            this.temp = temp;
            this.humidity = humidity;
            this.pressure = pressure;

            measurementsChanged();
        }
    }
}
