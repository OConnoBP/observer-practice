﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace observer_practice
{
    public class AverageConditionsDisplay: IObserver, IDisplay
    {
        private List<float> temps;
        private List<float> humidities;
        private List<float> pressures;
        private ISubject weatherData;

        public AverageConditionsDisplay( ISubject weatherData )
        {
            this.weatherData = weatherData;
            this.temps = new List<float>();
            this.humidities = new List<float>();
            this.pressures = new List<float>();

            this.weatherData.registerObserver( this );
        }

        public void display()
        {
            Console.WriteLine($"Average temp is: {this.getAverage(temps)}, Average humidity is: {this.getAverage(humidities)}, Average pressure is: {this.getAverage(pressures)}");
        }

        public void update(float temp, float humidity, float pressure)
        {
            this.temps.Add(temp);
            this.humidities.Add(humidity);
            this.pressures.Add(pressure);
            display();
        }

        private float getAverage(IList<float> list)
        {
            float total = 0;
            foreach(float number in list)
            {
                total += number;
            }

            return total / list.Count();
        }
    }
}
