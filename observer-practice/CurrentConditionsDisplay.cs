﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace observer_practice
{
    public class CurrentConditionsDisplay : IDisplay, IObserver
    {
        private float temp;
        private float humidity;
        private ISubject weatherData;

        public CurrentConditionsDisplay( ISubject weatherData )
        {
            this.weatherData = weatherData;
            this.weatherData.registerObserver( this );
        }

        public void update(float temp, float humidity, float pressure)
        {
            this.temp = temp;
            this.humidity = humidity;
            display();
        }

        public void display()
        {
            Console.WriteLine($"Current condition: {this.temp}F degrees and {this.humidity}% humidity");
        }
    }
}
